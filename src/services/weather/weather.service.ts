import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable()
export class WeatherService {
  
  constructor(
    private HttpClient: HttpClient
  ) {
  }

  public getWeatherData(province:string): Observable<any> {
    const url: string = environment.api.url + province + '&appid=' + environment.api.weatherKey + '&lang=es&units=metric' ;
    return this.HttpClient.get<any>(url).pipe(
      map((response) => {
        return response;
      })
    );
  }
}
