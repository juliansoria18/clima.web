import { AfterViewInit, ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { City } from 'src/models/city.model';
import { WeatherService } from 'src/services/weather/weather.service';
import { NgxSpinnerService } from "ngx-spinner";
import { HttpErrorResponse } from '@angular/common/http';
import { MatCheckboxChange } from '@angular/material/checkbox';
import { MatTable } from '@angular/material/table';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent implements OnInit,AfterViewInit {
  //columnas de la tabla
  displayedColumns: string[] = ['nacionality', 'city', 'temp', 'feels_like','date'];
  //tabla
  @ViewChild(MatTable)
  table!: MatTable<any>;
  //source para ver valores historicos
  dataSource:any[] = [];
  //array de ciudades
  cities: City[] = []
  data:any[]=[]
  //ciudad seleccionada
  selected: any = null
  //ciudades para crear mock
  city0: City = { id: "id1", province: "Salta", nacionality: "Argentina" }
  city1: City = { id: "id2", province: "Buenos Aires", nacionality: "Argentina" }
  city2: City = { id: "id3", province: "Cordoba", nacionality: "Argentina" }
  city3: City = { id: "id4", province: "Formosa", nacionality: "Argentina" }
  //temperatura
  temp: number = 0
  //sensacion termica
  feels_like: number = 0
  //flag para visualizar detalle de la temperatura
  viewData = false
  //get valor checkbox
  checkedValue = false
  //duracion snackbar
  durationInSeconds = 5;
  constructor(
    private _weatherService: WeatherService,
    private _ngxSpinnerService: NgxSpinnerService,
    private _snackBar: MatSnackBar) 
    {
    this.createMock()
    
  }

  ngOnInit(): void {
    
  }

  ngAfterViewInit(){
  }

  createMock() {
    this.cities.push(this.city0)
    this.cities.push(this.city1)
    this.cities.push(this.city2)
    this.cities.push(this.city3)
  }

  consult() {
    this._ngxSpinnerService.show()
    this._weatherService.getWeatherData(this.selected.province).subscribe(response => {
      this._ngxSpinnerService.hide()
      this.viewData = true
      if(this.checkedValue){
        const value:any = {
          city : this.selected.province,
          nacionality : this.selected.nacionality,
          temp:response.main.temp,
          feels_like:response.main.feels_like,
          index : this.dataSource.length + 1,
          date : new Date()
        }
        this.dataSource = JSON.parse(localStorage.getItem(this.selected.province) || "[]");
        this.dataSource.push(value)
        this.dataSource.sort((one, two) => (one.index > two.index ? 1 : -1));
        localStorage.setItem(this.selected.province, JSON.stringify(this.dataSource));
        if(this.table){
          this.table.renderRows();
        }
      }else{
        this.dataSource = []
      }
      this.temp = response.main.temp
      this.feels_like = response.main.feels_like
    },
    (error: HttpErrorResponse) => {
      console.log('Error', error);
      this._ngxSpinnerService.hide()
      this.viewData = false
      this._snackBar.openFromComponent(SnackComponent, {
        duration: this.durationInSeconds * 1000,
      });
    })
  }

  checked(event: MatCheckboxChange){
    this.checkedValue = event.checked
  }

  selectCity(city: City) {
    this.selected = city
    this.viewData = false
    this.dataSource = []
  }
}

@Component({
  selector: 'snack-bar-component-example-snack',
  templateUrl: 'snack-bar.component.html',
  styles: [
    `
    .snack-view {
      color: white;
    }
  `,
  ],
})
export class SnackComponent {}
